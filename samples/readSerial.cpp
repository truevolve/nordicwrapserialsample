#include <iostream>
#include <iomanip>
#include "../include/ConnectionWrapper.h"

using namespace std;

int main() {
    auto cW = ConnectionWrapper();
    
    cW.openConnection("/dev/ttyACM0", 18);

    auto serial = cW.getSerial();
    cout << "Read the following serial from device: [" << serial.getResponse() << "]" << endl;
    cout << "Read the following error: [" << serial.getError() << "]" << endl << endl;
    
    cW.closeConnection();
}