# Nordic Wrap Sample

This repo exists to showcase a the specific problem in using the NUR_API in a windows environment when trying to display the serial code.

# Context

We provide a wrapper for different RFID readers that looks the same regardless of what physical reader is actually used.
These wrappers are written in C++11 (making sure not to use any platform/architecture specific headers/APIs) and we use
Swig to provide easy-to-use interfaces in Java, Golang and C#. We use all of these wrappers in all of the mentioned languages on Windows and Linux (x86_64 and arm) in multiple different applications and services.
One of our clients depends on the serial number to identify work stations. Therefore it is important for us to be able to read the serial number of the Stix device via the C API.
In Linux, we are using GCC/G++ to compile, and for Windows, we also use GCC/G++ via MinGW. For both these cases, we use an Ubuntu-based docker image to compile/cross-compile.

# Building

The pipelines does the building of the project and makes a sample available under downloads. The pipelines instructions can be found in 'bitbucket-pipelines.yaml'.

# Description of the problem

Initially the issue was that when calling NurApiGetReaderInfo, the struct NUR_READERINFO was not being populated at all.
This was resolved by adding the following lines (lines 39 & 40) to the CMakeLists.txt:

~~~
    set(CMAKE_CXX_STANDARD 11 "${CMAKE_CXX_FLAGS} -fexec-charset=utf-16")
    add_definitions(-DUNICODE -D_UNICODE -D_WIN32 -DUNICODE)
~~~

After the sample has been built with the above mentioned lines, the architecture appropriate executable is run on both linux and on windows. In the case of Linux, the serial is returned and displayed with no issues.
However, in the windows case, the serial number is not returned properly, instead, what appears to be a memory address is returned, and there is no error displayed.