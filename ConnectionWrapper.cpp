/*
 * Author: Vincent Mattana
 *
 * Created on 31 Aug 2018
 */

#include <unistd.h>
#include <chrono>
#include <thread>
#include "include/ConnectionWrapper.h"

/**
 * Convert null terminated TCHAR string to std::string.
 * @param str Pointer to NULL terminated TCHAR string.
 * @return Converted std::string
 */
static std::string GetNurApiString(const TCHAR *str)
{
#ifdef _UNICODE
    // Create std wide string from UTF-16 string (win32 api WCHAR)
    std::wstring wSerialStr(str);
    // Convert std::wstring to std::string
    return std::string( wSerialStr.begin(), wSerialStr.end() );
#else
    // Standard ascii string
    return std::string(str);
#endif
}

/**
 * Convert Nu8rApi error code to std::string.
 * @param error NurApi error code.
 * @return Human readable error string
 */
static std::string GetNurApiErrorString(int error)
{
    TCHAR errorMsg[128 + 1]; // 128 + NULL
    NurApiGetErrorMessage(error, errorMsg, 128);
    std::string errorMsgStr = GetNurApiString(errorMsg);
    return errorMsgStr;
}

/**
 * Opens a connection:
 * Used to open a connection to the reader, it requires a valid IP
 * @param connectionString A String specifying a serial device eg: "/dev/ttyAMC0", on Windows machines this is not used because NurApiSetUsbAutoConnect is avaialable
 * @return ResponseObject Error: "OK"
 */
ResponseObject ConnectionWrapper::openConnection(std::string connectionString, int power) {
    ResponseObject myResponse;
    int error = 0;
    this->power = power;

    hApi = NurApiCreate();
    if (hApi == INVALID_HANDLE_VALUE) {
        loge("Could not create NurApi object.");
    }

    // Connect API to module through serial port
    logd("Opening the connection");

#ifdef WIN32
    error = NurApiSetUsbAutoConnect(hApi, TRUE);
#else
    error = NurApiConnectSerialPortEx(hApi, connectionString.data(), NUR_DEFAULT_BAUDRATE);
#endif
    if (error != NUR_NO_ERROR && error != NUR_ERROR_NO_TAG) {
        std::string errorMsgStr(GetNurApiErrorString(error));
        loge("NurApi error " << error << ": " << errorMsgStr);
        myResponse.setError(errorMsgStr);
        return myResponse;
    }

    struct NUR_MODULESETUP setup{};
    setup.txLevel = power;
    //set antenna power levels:
    error = NurApiSetModuleSetup(hApi, NUR_SETUP_TXLEVEL, &setup, sizeof(setup));
    if (error != NUR_NO_ERROR) {
        std::string errorMsgStr(GetNurApiErrorString(error));
        loge("NurApi error " << error << ": " << errorMsgStr);
        myResponse.setError(errorMsgStr);
        return myResponse;
    }

    myResponse.setError(OK);
    return myResponse;
}

ResponseObject ConnectionWrapper::getSerial() {
    ResponseObject myResponse;
    auto *readerInfo = new NUR_READERINFO();

    auto error = NurApiGetReaderInfo(hApi, readerInfo, sizeof(*readerInfo));
    if (error != 0) {
        std::string errorMsgStr(GetNurApiErrorString(error));
        loge("NurApi error " << error << ": " << errorMsgStr);
        myResponse.setError(errorMsgStr);
    } else {
        myResponse.setResponse(GetNurApiString(readerInfo->serial));
        myResponse.setError(OK);
    }

    return myResponse;
}

ResponseObject ConnectionWrapper::closeConnection() {
    logd("Closing the connection");
    ResponseObject myResponse;

    auto error = NurApiFree(hApi);
    if (error != NUR_NO_ERROR) {
        std::string errorMsgStr(GetNurApiErrorString(error));
        loge("NurApi error " << error << ": " << errorMsgStr);
        myResponse.setError(errorMsgStr);
        return myResponse;
    }

    myResponse.setError(OK);
    myResponse.setResponse("The connection is now closed");
    return myResponse;
}

ConnectionWrapper::ConnectionWrapper() = default;