cmake_minimum_required(VERSION 3.9)
project(NordicWrap)

set(CMAKE_CXX_STANDARD 11)

if (CMAKE_BUILD_TYPE STREQUAL "Release")
    add_definitions(-DLOG_LEVEL_ERROR)
else ()
    add_definitions(-DLOG_LEVEL_DEBUG)
endif ()

include_directories(include)

set(SOURCE_FILES
        include/NurOS.h
        include/NurAPIExport.h
        include/NurAccessoryExtension.h
        include/NurAPI.h
        include/NurAPIConstants.h
        include/NurAPIErrors.h
        include/logdefines.h
        include/errorCodes.h
        include/version.h
        include/ResponseObject.h
        include/ConnectionWrapper.h
        ConnectionWrapper.cpp
        ResponseObject.cpp)

add_library(NordicWrap SHARED ${SOURCE_FILES})

add_library(NordicWrap_java SHARED ${SOURCE_FILES} swig/NordicWrap_java.cpp)
add_library(NordicWrap_csharp SHARED ${SOURCE_FILES} swig/NordicWrap_csharp.cpp)

add_executable(readSerial samples/readSerial.cpp)
target_link_libraries(readSerial NordicWrap)


if (MSVS OR MSYS OR MINGW)
    set(CMAKE_CXX_STANDARD 11 "${CMAKE_CXX_FLAGS} -fexec-charset=utf-16")
    add_definitions(-DUNICODE -D_UNICODE -D_WIN32 -DUNICODE)
    target_link_libraries(NordicWrap ${CMAKE_CURRENT_SOURCE_DIR}/libs/windows/x64/NURAPI.lib ${CMAKE_CURRENT_SOURCE_DIR}/libs/windows/x64/NURAPI.dll)
    target_link_libraries(NordicWrap_java ${CMAKE_CURRENT_SOURCE_DIR}/libs/windows/x64/NURAPI.lib ${CMAKE_CURRENT_SOURCE_DIR}/libs/windows/x64/NURAPI.dll)
    target_link_libraries(NordicWrap_csharp ${CMAKE_CURRENT_SOURCE_DIR}/libs/windows/x64/NURAPI.lib ${CMAKE_CURRENT_SOURCE_DIR}/libs/windows/x64/NURAPI.dll)

    set_target_properties(NordicWrap PROPERTIES SUFFIX ".dll")
    set_target_properties(NordicWrap_java PROPERTIES SUFFIX ".dll")
    set_target_properties(NordicWrap_csharp PROPERTIES SUFFIX ".dll")

    set_target_properties(readSerial PROPERTIES SUFFIX ".exe")
else ()
    target_link_libraries(NordicWrap ${CMAKE_CURRENT_SOURCE_DIR}/libs/linux/x64/libNurApix64.so -lm -lpthread)
    target_link_libraries(NordicWrap_java ${CMAKE_CURRENT_SOURCE_DIR}/libs/linux/x64/libNurApix64.so -lm -lpthread)
    target_link_libraries(NordicWrap_csharp ${CMAKE_CURRENT_SOURCE_DIR}/libs/linux/x64/libNurApix64.so -lm -lpthread)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-rpath='$ORIGIN'")
endif ()