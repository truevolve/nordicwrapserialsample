/*
 * Author: Vincent Mattana
 *
 * Created on 31 Aug 2018
 */

#ifndef CUSTOMNORDICIDSTIX_CONNECTIONWRAPPER_H
#define CUSTOMNORDICIDSTIX_CONNECTIONWRAPPER_H

#include <sstream>
#include <memory>
#include <iomanip>
#include <string>
#include "logdefines.h"
#include "ResponseObject.h"
#include "version.h"
#include "NurAPI.h"
#include "errorCodes.h"


/**
 *  @brief
 *  The ConnectionWrapper communicates directly with the Kathrein RFID reader.
 *
 *  It is important to note that the ConnectionWrapper only implements reader specific commands, ie for controlling the reader, not programming the tags.
 *  The wrapping layer offers a standard interface to communicate with the library from both Java and C++.
 *   All of the underlying functionality is exposed through the wrapper and returned as a ResponseObject. The idea is to abstract the inner workings (callback registration and handling) of the library from the user and present a standard interface to higher level programming languages such as Java.
 *
 */
class
#ifdef _WINDOWS
    __declspec(dllexport)
#endif
ConnectionWrapper {
protected:
public:
    ConnectionWrapper();

    ResponseObject openConnection(std::string connectionString, int power);

    ResponseObject getSerial();

    ResponseObject closeConnection();

private:
    int timeout = 100;

    /* 500mW reader values:
    * @li 0 = 27 dBm, 500mW
    * @li 1 = 26 dBm, 398mW
    * @li 2 = 25 dBm, 316mW
    * @li 3 = 24 dBm, 251mW
    * @li 4 = 23 dBm, 200mW
    * @li 5 = 22 dBm, 158mW
    * @li 6 = 21 dBm, 126mW
    * @li 7 = 20 dBm, 100mW
    * @li 8 = 19 dBm, 79mW
    * @li 9 = 18 dBm, 63mW
    * @li 10 = 17 dBm, 50mW
    * @li 11 = 16 dBm, 40mW
    * @li 12 = 15 dBm, 32mW
    * @li 13 = 14 dBm, 25mW
    * @li 14 = 13 dBm, 20mW
    * @li 15 = 12 dBm, 16mW
    * @li 16 = 11 dBm, 13mW
    * @li 17 = 10 dBm, 10mW
    * @li 18 = 9 dBm, 8mW
    * @li 19 = 8 dBm, 6mW
     */
    int power = 0;

    std::string version = VERSION_TEXT;
    HANDLE hApi = INVALID_HANDLE_VALUE;
};

#endif //CUSTOMNORDICIDSTIX_CONNECTIONWRAPPER_H
