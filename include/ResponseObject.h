/*
 * Author: Vincent Mattana
 *
 * Created on 31 Aug 2018
 */

#ifndef CUSTOMNORDICIDSTIX_RESPONSEOBJECT_H
#define CUSTOMNORDICIDSTIX_RESPONSEOBJECT_H

#include <string>

class
#ifdef _WINDOWS
    __declspec(dllexport)
#endif
ResponseObject {

public:
    void setError(std::string err);
    void setResponse(std::string response);
    std::string getError();
    std::string getResponse();
    ResponseObject();

    virtual ~ResponseObject();

private:

protected:
    std::string errorCode;
    std::string responseValue;

};

#endif //CUSTOMNORDICIDSTIX_RESPONSEOBJECT_H
