/*
 * File:   logdefines.h
 * Author: Steyn Geldenhuys
 *
 * Created on September 1, 2016, 7:15 AM
 */

#ifndef LOGDEFINES_H
#define LOGDEFINES_H

#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#ifdef __ANDROID_API__

#include <android/log.h>

#define basecmd(x) stringstream ss1, ss2; \
                ss1<<__FILENAME__<<" @ "<<__LINE__ <<" "; \
                ss2<<x;

#define LOGD(x) { \
                    basecmd(x) \
                    __android_log_write(ANDROID_LOG_DEBUG, ss1.str().c_str(), ss2.str().c_str()); \
                }

#define LOGI(x) { \
                    basecmd(x) \
                    __android_log_write(ANDROID_LOG_INFO, ss1.str().c_str(), ss2.str().c_str()); \
                }

#define LOGW(x) { \
                    basecmd(x) \
                    __android_log_write(ANDROID_LOG_WARN, ss1.str().c_str(), ss2.str().c_str()); \
                }

#define LOGE(x) { \
                    basecmd(x) \
                    __android_log_write(ANDROID_LOG_ERROR, ss1.str().c_str(), ss2.str().c_str()); \
                }
#else

#define LOGD(x) cout<< "DEBUG: "<<__FILENAME__<<" @ "<<__LINE__<< " " <<x<<endl;
#define LOGI(x) cout<<"INFO: "<<__FILENAME__<<" @ "<<__LINE__<< " " <<x<<endl;
#define LOGW(x) cout<<"WARN: "<<__FILENAME__<<" @ "<<__LINE__<< " " <<x<<endl;
#define LOGE(x) cout<<"ERROR: "<<__FILENAME__<<" @ "<<__LINE__<< " " <<x<<endl;

#endif /* else */

#if defined(LOG_LEVEL_DEBUG)

#define logd(x) LOGD(x)
#define logi(x) LOGI(x)
#define logw(x) LOGW(x)
#define loge(x) LOGE(x)

#elif defined(LOG_LEVEL_INFO)

#define logd(x)
#define logi(x) LOGI(x)
#define logw(x) LOGW(x)
#define loge(x) LOGE(x)

#elif defined(LOG_LEVEL_WARN)

#define logd(x)
#define logi(x)
#define logw(x) LOGW(x)
#define loge(x) LOGE(x)

#elif defined(LOG_LEVEL_ERROR)

#define logd(x)
#define logi(x)
#define logw(x)
#define loge(x) LOGE(x)

#else

#define logd(x)
#define logi(x)
#define logw(x)
#define loge(x)

#endif

#endif /* LOGDEFINES_H */